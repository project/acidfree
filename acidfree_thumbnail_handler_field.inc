<?php
class acidfree_thumbnail_handler_field extends views_handler_field_node {
  function construct() { parent::construct(); }

  function render($values) { 
    global $user;
    $node = node_load(array('nid' => $values->nid));
    return theme("acidfree_print_thumb_{$node->type}", $node);
  }
}
?>
