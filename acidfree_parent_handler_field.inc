<?php
class acidfree_parent_handler_field extends views_handler_field_node {
  function construct() { parent::construct(); }

  function render($values) { 
    global $user;
    $node = node_load(array('nid' => $values->nid));
    $parent = _acidfree_get_parent($node);

    if (! $parent || ! $parent->tid) {
	return t('(root)');
    }

    $term = taxonomy_get_term($parent->tid);
    $parent = _album_from_tid($parent->tid);

    return l(t($term->name), "node/{$parent->nid}");
  }
}
?>
