Acidfree Albums
Author - Vernon Mauery <vernon At mauery d0t com>
Contributor - Michael Heinz <michael@heinztrucking.com>

Copyright (C) 2005, 2006, 2007, 2008 Vernon Mauery
Additional contributions Copyright 2010-2011 (C) Michael Heinz

Licensed under the GPL v2.0

About
-----------------------------
Acidfree albums were designed with simplicity in mind.  There are as many
web based photo albums as there are people with digital pictures, yet when
Acidfree albums were written, they all were found lacking.  Some were too
fancy, others were too ugly, and others (gasp!) were not even written in php.

To this end, Acidfree albums were created.  This version is the fifth revision
of the albums.  While functionally the same in each version, this is the first
modular enough to be easily distributed and used by others.  Previous versions
have been in use by websites for several years now, but tailoring them into the
various websites was not easy.  But then I found Drupal and fell in love with
it.  But it too was missing a good photo album.  So here it is.


See the help directory for detailed information on recent changes and on
installing and configuring Acidfree.
