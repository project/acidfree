<?php

function acidfree_views_data()  {
  // Basic table information.

  // ----------------------------------------------------------------
  // acidfree table
  //  New group within Views called 'Acidfree'
  //  The group will appear in the UI in the dropdown tha allows you
  //  to narrow down which fields and filters are available.

  $data = array();
  $data['acidfree_album']['table']['group']  = t('Acidfree');

  // Let Views know that our table joins to the 'node'
  // base table. This means it will be available when listing
  // nodes and automatically make its fields appear.
  //
  // We also show up for node revisions.
  $data['acidfree_album']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'aid',
    ),
  );

  // Thumbnail		
  $data['acidfree_album']['thumb'] = array(
    'title' => t('Thumbnail'),
    'help' => t('Thumbnail for album.'),

    'field' => array(
      'handler' => 'acidfree_thumbnail_handler_field',
     ),
  );

  $data['acidfree_album']['tid'] = array(
    'title' => t('Parent'),
    'help' => t('Parent of album.'),

    'field' => array(
      'handler' => 'acidfree_parent_handler_field',
     ),
  );

  return $data;
}

function acidfree_views_handlers() {
  return array(
    'handlers' => array(
      'acidfree_parent_handler_field' => array(
         'parent' => 'views_handler_field_node',
       ),
      'acidfree_thumbnail_handler_field' => array(
         'parent' => 'views_handler_field_node',
       ),
    ),
  );
}
?>
