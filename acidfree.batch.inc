<?php
/**
 * Adapted from http://drupal.org/node/180528
 *
 */
function acidfree_batch_delete($nodes, $options2=NULL) {
  $batch = array(
    'operations' => array(
      array('acidfree_batch_delete_process', array($nodes, $options2)),
      ),
    'finished' => 'acidfree_batch_finished',
    'title' => t('Deleting Selected Images'),
    'init_message' => t('Batch Delete is starting.'),
    'progress_message' => '',
    'error_message' => t('Batch Delete has encountered an error.'),
    'file' => drupal_get_path('module', 'acidfree') . '/acidfree.batch.inc',
  );
  batch_set($batch);

  batch_process('');
}

function acidfree_batch_delete_process($nodes, $options2, &$context) {
  $limit = 10;

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = count($nodes)-1;
    $context['sandbox']['nodes'] = $nodes;
  }

  $end = min($limit,count($context['sandbox']['nodes']));

  for ($i=0; $i<$end; $i++) {
    $node = array_shift($context['sandbox']['nodes']);
    // Here we actually perform our processing on the current node.
    node_delete($node->nid);
    //$context['results'][] = check_plain($node->title);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current_node'] = $node->nid;
    $context['message'] = t('Now deleting %node', array('%node' => $node->title));
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

function acidfree_batch_import_process($files, $node_items, $parent, $ltmpdir, &$context) {
  $limit = 2;

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = count($files);
    $context['sandbox']['files'] = $files;
  }

  $end = min($limit,count($context['sandbox']['files']));

  for ($i=0; $i<$end; $i++) {
    $file = array_shift($context['sandbox']['files']);
 
    drupal_set_message(print_r($file,1));

/*
    if (!$file->is_dir) {
        _acidfree_node_from_file($parent, $file, $node_items, $ltmpdir);
    } else {
    	$context['results'][] = clean_text(t('Error: Cannot import %file',$file->name));
    }
*/

    $context['results'][] = print_r($file,1);

    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['message'] = t('Now importing %file', array('%file' => $file->filename));
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

function acidfree_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(theme('item_list', $results));
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation.', 
	array('%error_operation' => $error_operation[0]));
    drupal_set_message($message, 'error');
    drupal_set_message(print_r($results,1), 'error');
  }
}
?>
